

openerp.barcode_customized = function(instance, module) { // module is
                                                            // instance.stock
    var module = instance.stock;
    var QWeb = instance.web.qweb;
    _t = instance.web._t;
    
    
    module.PickingMainWidget.include({


    init: function(parent,params){
                this._super(parent,params);
                var self = this;
                $(window).bind('hashchange', function(){
                    var states = $.bbq.getState();
                    if (states.action === "stock.menu"){
                        self.do_action({
                            type:   'ir.actions.client',
                            tag:    'stock.menu',
                            target: 'current',
                        },{
                            clear_breadcrumbs: true,
                        });
                    }
                });
                init_hash = $.bbq.getState();
                this.picking_type_id = init_hash.picking_type_id ? init_hash.picking_type_id:0;
                this.picking_id = init_hash.picking_id ? init_hash.picking_id:undefined;
                this.picking = null;
                this.pickings = [];
                this.packoplines = null;
                this.selected_operation = { id: null, picking_id: null};
                this.packages = null;
                this.barcode_scanner = new module.BarcodeScanner();
                this.locations = [];
                this.uls = [];
                if(this.picking_id){
                    this.loaded =  this.load(this.picking_id);
                }else{
                    this.loaded =  this.load();
                }

            },
            
        set_operation_quantity: function(quantity, op_id)
            {
                var self = this;
                if(quantity >= 0)
                {
                    return new instance.web.Model('stock.pack.operation')
                    .call('read',[[parseInt(op_id)], ['qty_done','product_id'], new instance.web.CompoundContext()]).
                    then(function(qty_read)
                        {
                                return new instance.web.Model('stock.pack.operation')
                            .call('write',[[op_id],{'qty_done': quantity }])
                            .then(function()
                        {
                             return new instance.web.Model('stock.pack.operation')
                            .call('search_read',[[['lot_id','=',false],['picking_id','=',self.picking.id],['product_id','=',qty_read[0]['product_id'][0]]],['product_qty']], {context: new instance.web.CompoundContext()})
                            .then(function(operation_id)
                            {
                                return new instance.web.Model('stock.pack.operation')
                                .call('write',[operation_id[0]['id'],{'product_qty': operation_id[0]['product_qty'] - (quantity-qty_read[0]['qty_done']) }])
                                .then(function()
                                {
                                    self.refresh_ui(self.picking.id);
                                });
                            });
                        });
                    });
                };
            },

        scan: function(ean){ // scans a barcode, sends it to the server, then
                                // reload the ui
            var self = this;
            var product_visible_ids = this.picking_editor.get_visible_ids();
            // var check_output = self.check_qty(ean);
            lot_search = new instance.web.Model('stock.production.lot')
                .call('search',[[['name','=',ean]]])
                .then(function(lot_ids)
               {
                if (lot_ids.length !== 0)
                    {
                        var lot_read = new instance.web.Model('stock.production.lot')
                        .call('read',[[lot_ids[0]], [], new instance.web.CompoundContext()])
                        .then(function(lot_read)
                                {
                                if(lot_read[0]['remaining_qty'] <= 0){
                                    (new instance.web.Dialog(self,
                                            {
                                            title: _t('No Item Available'),
                                            }, _t('<p>There is no Items available in this serial number.</p>'))
                                        ).open();
                                    // return;
                                  }
                                  else
                                  {
            return new instance.web.Model('stock.picking')
                .call('process_barcode_from_ui', [self.picking.id, ean, product_visible_ids])
                .then(function(result){
                    if (result.filter_loc !== false){
                        // check if we have receive a location as answer
                        if (result.filter_loc !== undefined){
                            var modal_loc_hidden = self.$('#js_LocationChooseModal').attr('aria-hidden');
                            if (modal_loc_hidden === "false"){
                                var line = self.$('#js_LocationChooseModal .js_loc_option[data-loc-id='+result.filter_loc_id+']').attr('selected','selected');
                            }
                            else{
                                self.$('.oe_searchbox').val(result.filter_loc);
                                self.on_searchbox(result.filter_loc);
                            }
                        }
                    }
                    if (result.operation_id !== false){
                        self.refresh_ui(self.picking.id).then(function(){
                            return self.picking_editor.blink(result.operation_id);
                        });
                    }
                });
                                  }
                            });
                    }
                });
        },
    });
    
module.PickingEditorWidget.include({
    	
        init: function(parent,options){
            this._super(parent,options);
            var self = this;
            this.rows = [];
            this.search_filter = "";
            jQuery.expr[":"].Contains = jQuery.expr.createPseudo(function(arg) {
                return function( elem ) {
                    return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
                };
            });
        },
        get_rows: function(){
            var model = this.getParent();
            this.rows = [];
            var self = this;
            var pack_created = [];
            _.each( model.packoplines, function(packopline){
                    var pack = undefined;
                    var color = "";
                    if (packopline.product_id[1] !== undefined){ pack = packopline.package_id[1];}
                    if (packopline.product_qty == packopline.qty_done){ color = "success "; }
                    //also check that we don't have a line already existing for that package
                    if (packopline.result_package_id[1] !== undefined && $.inArray(packopline.result_package_id[0], pack_created) === -1){
                        var myPackage = $.grep(model.packages, function(e){ return e.id == packopline.result_package_id[0]; })[0];
                        self.rows.push({
                            cols: { product: packopline.result_package_id[1],
                                    qty: '',
                                    rem: '',
                                    uom: undefined,
                                    lot: undefined,
                                    pack: undefined,
                                    container: packopline.result_package_id[1],
                                    container_id: undefined,
                                    loc: packopline.location_id[1],
                                    dest: packopline.location_dest_id[1],
                                    id: packopline.result_package_id[0],
                                    product_id: undefined,
                                    can_scan: false,
                                    head_container: true,
                                    processed: packopline.processed,
                                    package_id: myPackage.id,
                                    ul_id: myPackage.ul_id[0],
                            },
                            classes: ('success container_head ') + (packopline.processed === "true" ? 'processed hidden ':''),
                        });
                        pack_created.push(packopline.result_package_id[0]);
                    }
                    self.rows.push({
                        cols: { product: packopline.product_id[1] || packopline.package_id[1],
                                qty: packopline.product_qty,
                                rem: packopline.qty_done,
                                uom: packopline.product_uom_id[1],
                                lot: packopline.lot_id[1],
                                pack: pack,
                                container: packopline.result_package_id[1],
                                container_id: packopline.result_package_id[0],
                                loc: packopline.location_id[1],
                                dest: packopline.location_dest_id[1],
                                id: packopline.id,
                                product_id: packopline.product_id[0],
                                can_scan: packopline.result_package_id[1] === undefined ? true : false,
                                head_container: false,
                                processed: packopline.processed,
                                package_id: undefined,
                                ul_id: -1,
                        },
                        classes: color + (packopline.result_package_id[1] !== undefined ? 'in_container_hidden ' : '') + (packopline.processed === "true" ? 'processed hidden ':''),
                    });
            });
            //sort element by things to do, then things done, then grouped by packages
            group_by_container = _.groupBy(self.rows, function(row){
                return row.cols.container;
            });
            var sorted_row = [];
            if (group_by_container.undefined !== undefined){
                group_by_container.undefined.sort(function(a,b){return (b.classes === '') - (a.classes === '');});
                $.each(group_by_container.undefined, function(key, value){
                    sorted_row.push(value);
                });
            }
        
            $.each(group_by_container, function(key, value){
                if (key !== 'undefined'){
                    $.each(value, function(k,v){
                        sorted_row.push(v);
                    });
                }
            });
    return sorted_row;
    },

    get_current_op_selection: function(ignore_container){
        //get ids of visible on the screen
        pack_op_ids = []
        this.$('.js_pack_op_line:not(.processed):not(.js_pack_op_line.hidden):not(.container_head)').each(function(){
            cur_id = $(this).data('id');
            pack_op_ids.push(parseInt(cur_id));
        });
        //get list of element in this.rows where rem > 0 and container is empty is specified
        list = []
        _.each(this.rows, function(row){
                list.push(row.cols.id);
        });
        //return only those visible with rem qty > 0 and container empty
        return _.intersection(pack_op_ids, list);
    },
	});

};
